from scipy.spatial import distance_matrix
import utils
import numpy as np
import pandas as pd
from class_knn_optimisation import KNN

#load data
path = "C://Users//Victor//Desktop//devoir eurecom//2023-2024//malis//projet1//data//training.csv"
path_validation = "C://Users//Victor//Desktop//devoir eurecom//2023-2024//malis//projet1//data//validation.csv"
data_train = pd.read_csv(path)

X_train = data_train[['X1', 'X2']]
Y_train = data_train['y']

# Create the meshgrid
h = 0.05
x_min, x_max = X_train['X1'].min() - 1, X_train['X1'].max() + 1
y_min, y_max = X_train['X2'].min() - 1, X_train['X2'].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
X_test_grid = np.c_[xx.ravel(), yy.ravel()]

# Create the knn
knn = KNN(k=39)
knn.train(X_train, Y_train)
y_pred_grid = knn.predict_opt(X_test_grid, 2)
y_pred_grid = y_pred_grid.reshape(xx.shape)
X_np = X_train.to_numpy()
Y_np = Y_train.to_numpy()

utils.plot_results(xx, yy, X_np, Y_np, y_pred_grid, f"k={knn.k}")


        




        




            
