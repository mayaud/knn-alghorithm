""" First prototype of the KNN."""

from scipy.spatial import distance_matrix
import numpy as np
import pandas as pd
from collections import Counter
from collections.abc import Iterable  

class KNN:
    '''
    k nearest neighboors algorithm class
    __init__() initialize the model
    train() trains the model
    predict() predict the class for a new point
    '''

    def __init__(self, k):
        '''
        INPUT :
        - k : is a natural number bigger than 0 
        '''

        if k <= 0:
            raise Exception("k has to be superior to 0")
            
        # empty initialization of X and y
        self.X = []
        self.y = []
        # k : the number of neighborhoods
        self.k = k
        
    def train(self,X,y):
        '''
        INPUT :
        - X : is a 2D NxD numpy array containing the coordinates of points
        - y : is a 1D Nx1 numpy array containing the labels for the corrisponding row of X
        ''' 

        #implement the data in X and y.        
        self.X=np.array(X)
        self.y=np.array(y)

    def predict(self,X_new,p):
        '''
        INPUT :
        - X_new : is a MxD numpy array containing the coordinates of new points whose label has to be predicted
        A
        OUTPUT :
        - y_hat : is a Mx1 numpy array containing the predicted labels for the X_new points
        ''' 

        dst=self.minkowski_dist(X_new,p) 
        y_hat=[]
        M,N=dst.shape
        progress_interval = 100  # every 100 iterations we print the state

        for i in range(M):

            if i % progress_interval == 0:
                print(f"Processing {i}/{M}...")  

            liste_index=[]
            for k in range(N):
                liste_index.append((dst[i][k],k)) #we add in the list the distances and the corresponding index

            #we sort the list according to the distances and we take only the interesting neighbors
            liste_index  = sorted(liste_index, key=lambda x: x[0])
            liste_index = liste_index[0:k]

            y=[] #list of the output
            for k in range(self.k):
                    value = self.y[liste_index[k][1]]
    
                    # Check if the value is an array or list
                    if isinstance(value, Iterable) and not isinstance(value, str):
                        # If it's an array or list, we take the first value.
                        # This avoid to have a one-dimensional array with a single element
                        value = value[0]
                        
                    y.append(value)


            """To identify the most repeated label efficiently, I consulted ChatGPT regarding suitable libraries,
            and it was suggested that the collections library would be apt for this purpose.
            This library's Counter method was employed on the label array y to create a dictionary that tallies the occurrences of each label.
            We determine the most common label using the most_common function, which helps in finalizing the prediction."""
            
            #chatgpt
            compte = Counter(np.array(y))
            #return a dictionary which count the repetition of term
            max_occurrences = compte.most_common(1)[0][1]
            # find the color with the maximum occurence
            couleurs_max = [couleur for couleur, nb in compte.items() if nb == max_occurrences]
            #end of chatgpt
            y_hat.append(couleurs_max[0])

        return y_hat
    
    def minkowski_dist(self,X_new,p):
        '''
        INPUT : 
        - X_new : is a MxD numpy array containing the coordinates of points for which the distance to the training set X will be estimated
        - p : parameter of the Minkowski distance
        
        OUTPUT :
        - dst : is an MxN numpy array containing the distance of each point in X_new to X
        '''
        X_new=np.array(X_new)
        M, D = X_new.shape
        N, _ = self.X.shape

        dst=np.zeros((M,N))
        progress_interval=100 # every 100 iterations we print the state
        
        for i in range(N):
            if i % progress_interval == 0:
                print(f"Processing {i}/{N}...")  
            for m in range(M):
                distance=0
                for d in range(D):
                    distance+=(np.abs(X_new[m,d]-self.X[i,d]))**p
                dst[m][i]=distance**(1/p)
        return dst
