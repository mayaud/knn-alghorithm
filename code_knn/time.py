
import pandas as pd
import class_knn_optimisation as knn_opti
import class_knn as knn
import time
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier

#loading data
path ="C://Users//Victor//Desktop//devoir eurecom//2023-2024//malis//projet1//data//training.csv"
path_validation ="C://Users//Victor//Desktop//devoir eurecom//2023-2024//malis//projet1//data//validation.csv"
data_test = pd.read_csv(path)
data_validation=pd.read_csv(path_validation)

X_train = data_test[['X1', 'X2']]
Y_train = data_test[['y']]
X_validation = data_validation[['X1', 'X2']]
Y_validation = data_validation[['y']]
Y_validation = Y_validation.to_numpy()

knn_no_optimisation= knn.KNN(10)
knn_no_optimisation.train(X_train,Y_train)
start_time = time.time()
y_pred = knn_no_optimisation.predict(X_validation, 2)
end_time =time.time()
time_no_optimisation= end_time-start_time

knn_optimisation= knn_opti.KNN(10)
knn_optimisation.train(X_train,Y_train)
start_time = time.time()
y_pred = knn_optimisation.predict_opt(X_validation, 2)
end_time =time.time()
time_optimisation= end_time-start_time

knn_sklearn = KNeighborsClassifier(n_neighbors=10)
start_time_sklearn = time.time()
knn_sklearn.fit(X_train.to_numpy(), Y_train.to_numpy().ravel())
y_pred_sklearn = knn_sklearn.predict(X_validation.to_numpy())
end_time_sklearn = time.time()
time_sklearn = end_time_sklearn - start_time_sklearn


times = [time_no_optimisation, time_optimisation, time_sklearn]
labels = ['Without Optimisation', 'With Optimisation', 'Scikit-Learn']

print("time_no_optimisation : ", time_no_optimisation , ", time_optimisation : ",  time_optimisation , ", time_sklearn :",time_sklearn)

plt.bar(labels, times, color=['blue', 'green', 'red'])

plt.title('kNN Execution Time Comparison')
plt.ylabel('Time (seconds)')
plt.xlabel('Methods')

plt.show()