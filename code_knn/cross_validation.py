#Goal find the best value of k

import numpy as np
import pandas as pd
from collections import Counter
from collections.abc import Iterable  
from class_knn_optimisation import *
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier


def separation(X, Y, n):
    """
    Divides the dataset into `n` parts, ensuring each part except possibly the last has equal data.
    
    Parameters:
    X -- features dataset.
    Y -- labels dataset.
    n -- number of parts to divide into.
    
    Returns:
    A tuple of two lists: partitioned features and labels.
    """

    m = len(Y)
    number_data = m // n
    X_list = []
    Y_list = []
    for k in range(n):
        if k == (n-1):
            X_list.append(X[k*number_data:])
            Y_list.append(Y[k*number_data:])  
        else:
            X_list.append(X[k*number_data:(k+1)*number_data])  
            Y_list.append(Y[k*number_data:(k+1)*number_data])  
    return X_list, Y_list


def cross_validation(X_train,Y_train,n_partition,k_max):
    """
    Performs cross-validation to find the best hyperparameter k.
    
    Parameters:
    X_train -- training features.
    Y_train -- training labels.
    n_partitions -- number of folds for cross-validation.
    k_max -- maximum value of k to try.
    
    Returns:
    The best k value found through cross-validation.
    """

    # Partition the training data
    X_list,Y_list=separation(X_train,Y_train,n_partition)

    #initialisation of an dictionnary in order to keep the total precision of each k
    k_accuracies = {k: [] for k in range(1, k_max, 2)}    
    k_range = range(1, k_max, 2) 

    #create the knn
    for partition in range(n_partition): 
        # Create training and validation sets for the current fold
        X_train = np.concatenate([X_list[i] for i in range(n_partition) if i != partition])
        Y_train = np.concatenate([Y_list[i] for i in range(n_partition) if i != partition])
        X_val = X_list[partition]
        Y_val = np.array(Y_list[partition])
        
        # Evaluate KNN for each odd value of k
        for k in k_range:
            knn= KNN(k)
            knn.train(X_train,Y_train)
            y_pred = knn.predict_opt(X_val, 2)

            # Calculate and record the accuracy
            true_prediction = sum(y_pred_val == y_val_val for y_pred_val, y_val_val in zip(y_pred, Y_val))
            accuracy_value = true_prediction / len(Y_val)
            k_accuracies[k].append(accuracy_value)

    # Calculate the average accuracy for each k and determine the best k
    k_average_accuracies = {k: np.mean(acc) for k, acc in k_accuracies.items()}
    best_k = max(k_average_accuracies, key=k_average_accuracies.get)

    return best_k

def validation(X_train,Y_train,X_validation,Y_validation,k):
    """
    Validates the KNN model using the provided validation set.
    
    Parameters:
    X_train -- training features.
    Y_train -- training labels.
    X_val -- validation features.
    Y_val -- validation labels.
    k -- the hyperparameter for KNN.
    
    Returns:
    Accuracy of the KNN model on the validation data.
    """
    knn= KNN(k)
    knn.train(X_train,Y_train)
    y_pred = knn.predict_opt(X_validation, 2)
    true_prediction = sum(y_pred_val == y_val for y_pred_val, y_val in zip(y_pred, Y_validation))
    return  true_prediction / len(Y_validation)
    




#loading data
path ="C://Users//Victor//Desktop//devoir eurecom//2023-2024//malis//projet1//data//training.csv"
path_validation ="C://Users//Victor//Desktop//devoir eurecom//2023-2024//malis//projet1//data//validation.csv"
data_test = pd.read_csv(path)
data_validation=pd.read_csv(path_validation)

X_train = data_test[['X1', 'X2']]
Y_train = data_test[['y']]
X_validation = data_validation[['X1', 'X2']]
Y_validation = data_validation[['y']]
Y_validation = Y_validation.to_numpy()

#cross-valisation
k=cross_validation(X_train,Y_train,5,100)

#value of k
print(k)
print("The best value of k with cross-validation is : ", k)

#accuracy in validation test
accuracy=validation(X_train,Y_train,X_validation,Y_validation,k)
print("The accuracy in validation test is equal to : ", accuracy)

#Find the accuracy with scikit-learns library:
knn_sklearn = KNeighborsClassifier(n_neighbors=39)
knn_sklearn.fit(X_train.to_numpy(), Y_train.to_numpy().ravel())
y_pred_sklearn = knn_sklearn.predict(X_validation.to_numpy())
true_prediction = sum(y_pred_val == y_val for y_pred_val, y_val in zip(y_pred_sklearn, Y_validation))
sklearn_accuracy=true_prediction / len(Y_validation)
print("The accuracy in validation test with scikit-learns library is equal to : ", sklearn_accuracy)


#graph
accuracy=[]
k_values = range(1, 100, 2)

for i in k_values:
    current_accuracy = validation(X_train, Y_train, X_validation, Y_validation, i)
    accuracy.append(current_accuracy)

# Plot the accuracies over k values
plt.plot(k_values, accuracy)
plt.title('k-NN Accuracy over Different k Values')
plt.xlabel('k Value')
plt.ylabel('Accuracy')
plt.grid(True)
plt.show()
        