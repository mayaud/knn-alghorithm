"""Final prototype of KNN with optimisation. The knn used for all the tests."""

import numpy as np
from collections import Counter
from scipy.spatial.distance import cdist

class KNN:
    '''
    k nearest neighboors algorithm class
    __init__() initialize the model
    train() trains the model
    predict() predict the class for a new point
    '''

    def __init__(self, k):
        '''
        INPUT :
        - k : is a natural number bigger than 0 
        '''

        if k <= 0:
            raise Exception("k has to be superior to 0")
            
        # empty initialization of X and y
        self.X = []
        self.y = []
        # k : the number of neighborhoods
        self.k = k
        
    def train(self,X,y):
        '''
        INPUT :
        - X : is a 2D NxD numpy array containing the coordinates of points
        - y : is a 1D Nx1 numpy array containing the labels for the corrisponding row of X
        ''' 

        #implement the data in X and y.        
        self.X=np.array(X)
        self.y=np.array(y)

    def predict_opt(self, X_new, p):
        dst = self.minkowski_dist(X_new, p)
        y_hat = np.empty(X_new.shape[0], dtype=self.y.dtype)

        for i in range(X_new.shape[0]):
            # Sort the distances to get the indices of k nearest neighbors
            neighbors_idx = np.argsort(dst[i])[:self.k]

            # Flatten the array of nearest neighbor labels
            nearest_labels = self.y[neighbors_idx].flatten()

            label_counts = Counter(nearest_labels)
            y_hat[i] = label_counts.most_common(1)[0][0]

        return y_hat
    

    def minkowski_dist(self, X_new, p):
        """
        INPUT : 
        - X_new : is a MxD numpy array containing the coordinates of points for which the distance to the training set X will be estimated
        - p : parameter of the Minkowski distance
        
        OUTPUT :
        - dst : is an MxN numpy array containing the distance of each point in X_new to X
        """
        dst = cdist(X_new, self.X, metric='minkowski', p=p)
        return dst

