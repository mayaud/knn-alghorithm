# Projet k-NN : Implémentation et Analyse

Ce dépôt contient tous les fichiers nécessaires pour exécuter et analyser un algorithme k plus proches voisins (k-NN) implémenté de zéro. Ce projet a pour but de comprendre en profondeur le fonctionnement de l'algorithme k-NN, ses implications pratiques, et les défis associés à la malédiction de la dimensionnalité.

## Structure du Dépôt

- `knn.py` : Le fichier Python avec le squelette de l'implémentation k-NN. Complétez ce fichier pour inclure les fonctions `train`, `predict` et `minkowski_dist`.
- `training.csv` et `validation.csv` : Données utilisées pour l'entraînement et la validation du modèle.
- `experiment.ipynb` : Un notebook Jupyter contenant des routines pré-codées pour visualiser les données et les résultats des analyses.

## Objectifs du Projet

1. **Compréhension des Mécanismes** : Apprendre le fonctionnement interne de l'algorithme k-NN.
2. **Gestion de la Dimensionnalité** : Explorer les défis liés à la malédiction de la dimensionnalité et leur impact sur les performances de k-NN.
3. **Expérience Pratique** : Acquérir de l'expérience dans la formation, la validation et les tests de modèles en utilisant Python et des bibliothèques courantes en apprentissage automatique.

## Installation et Exécution

1. Clonez ce dépôt.
2. Assurez-vous que Python et Jupyter sont installés sur votre système.
3. Ouvrez le fichier `experiment.ipynb` dans Jupyter et suivez les instructions pour exécuter les analyses.

## Rapport

Un rapport d'une page est également inclus, détaillant la formation du modèle, les résultats obtenus sur l'ensemble de validation, et les stratégies utilisées pour choisir le paramètre k. Le rapport discute également de la façon dont la malédiction de la dimensionnalité affecte les k-NNs.